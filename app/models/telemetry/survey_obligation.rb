require './lib/format_validator'

class SurveyObligation < ActiveRecord::Base
  belongs_to :user
  belongs_to :survey_definition

  include SurveySnake::FormatValidator

  validate :validate_submission

  def validate_submission
    if self.submission
      return if validate_format(self.survey_definition.format, self.submission)
      errors.add(:submission, 'not valid')
    end
  end

  scope :due_this_week,
        -> do
          where(due_at: (Date.today.beginning_of_week..Date.today.end_of_week))
        end

  scope :not_submitted, -> { where('submitted_at IS NULL') }

  scope :submittable_now,
    -> { not_submitted.where('CURRENT_TIMESTAMP BETWEEN due_at AND expires_at') }

  scope :missed, -> { not_submitted.where('expires_at > ?', Date.today) }

  alias_attribute :definition, :survey_definition

  def self.obligations_due(week_index:)
    joins(:survey_definition)
      .where('? = ANY(survey_definitions.week_indices)', week_index)
  end

  def self.search_by_survey_name(survey_name)
      order('due_at ASC')
      .joins(:survey_definition)
      .where('survey_definitions.name' => survey_name).first
  end
end

=begin

Overview
1) A Survey Obligations ties a user and a survey definition together
2) They should exist as soon as a user exists. As such all surveys for all users exist when at the start, and users submit their answers as the weeks go by
3) There cannot be two surveys with the same definition and week index for one user, i.e {definition: 4, week_index: 3}, {definition: 4, week_index: 3} is illegal

Requirements
1) The due_at and expires_at are generated as a function of the team's program_starts_at, the week index, and the lateness_allowed
2) On submission the submission values must in compliance the definitions's format requirements
3) The submitted_at value must be generated when a valid submission is applied
4) Submissions must look like {k1: v1...kn: vn} where k and v correspond to this obligation's definition's format specification

=end
