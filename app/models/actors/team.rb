class Team < ActiveRecord::Base

  has_many :users
  before_save :normalise_program_starts_at

  validates_presence_of :program_starts_at
  validate :program_starts_at_in_future

  def normalise_program_starts_at
    self.program_starts_at = self.program_starts_at.beginning_of_week
  end

  def program_starts_at_in_future
    if program_starts_at < Time.now
      errors.add(:program_starts_at, 'must be greater than its creation time')
    end
  end

=begin
1) Teams have many users
2) Teams require a pathway_starts_at datetime greater than its creation time
3) The program_starts_at used when creating the model must be converted into the beginning of the week, i.e input_value.beginning_of_week
=end

end
