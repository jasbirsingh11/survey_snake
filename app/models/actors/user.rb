class User < ActiveRecord::Base

  belongs_to :team
  has_many :survey_obligations

  before_create :generate_access_token
  after_create :generate_survey_obligations

  private

  def generate_access_token
    self.token = Faker::Hipster.word
  end

  def generate_survey_obligations
    SurveyDefinition.all.each do |defn|
      defn.week_indices.each do |index|
        defn.survey_obligations.create(
          user_id: id,
          due_at: team.program_starts_at + (index + 1).week,
          expires_at: team.program_starts_at + (index + 1).week + defn.lateness_allowed.days
        )
      end
    end
  end
end
