class ApiController < ActionController::Base
  before_filter :authorise!

  def authorised
    render json: 'Successfully authorised.'
  end

  private

  def authorise!
    unless current_user
      render json: 'Unauthorised access.', status: 401
    end
  end

  def current_user
    User.find_by_token(params[:access_token])
  end
end
