require 'rails_helper'

describe User do
  let(:user) { FactoryGirl.create(:user_with_team) }
  let(:team) { user.team }

  before do
    FactoryGirl.create(:survey_definition)
  end

  describe "user's obligations" do
    it 'automatically generates survey obligations for the user' do
      expect(user.survey_obligations).to_not be_empty
    end
  end
end
