require 'rails_helper'

describe SurveysController, type: :controller do
  let(:user) { FactoryGirl.create(:user_with_team) }
  let(:user_obligations) { user.survey_obligations }
  let(:survey) { user.survey_obligations.first }

  before do
    @survey_definition = FactoryGirl.create(:survey_definition)
  end

  describe 'GET api/v1/surveys/:identifier' do
    it 'returns the single survey indentified by id' do
      get :show, id: survey.id, access_token: user.token

      expect(response.body).to eq(survey.to_json)
    end

    it "returns the single survey indentified by survey definitions's name" do
      survey.update(due_at: Date.today)

      get :show, id: survey.definition.name, access_token: user.token
      expect(response.body).to eq(survey.to_json)
    end
  end

  describe 'GET api/v1/surveys/due_this_week' do
    before do
      survey.update(due_at: Date.today)
    end

    it 'returns the list of surveys due this week' do
      get :due_this_week, access_token: user.token
      expect(response.body).to eq(user_obligations.due_this_week.to_json)
    end
  end

  describe 'api/v1/surveys/submittable_now' do
    before do
      survey.update(submitted_at: nil, due_at: Date.today - 1, expires_at: Date.today + 1)
    end

    it 'returns unsubmitted surveys for which the current time is between due_at and expires_at' do
      get :submittable_now, access_token: user.token

      expect(response.body).to eq(user_obligations.submittable_now.to_json)
    end
  end

  describe 'api/v1/surveys/missed' do
    before do
      survey.update(submitted_at: nil, expires_at: Date.today + 1)
    end

    it 'lists all surveys meant to be submitted but without submission' do
      get :missed, access_token: user.token

      expect(response.body).to eq(user_obligations.missed.to_json)
    end
  end

  describe 'api/v1/surveys/due/:week_index' do
    it 'lists surveys whose definitions week index matches :week_index' do
      index = @survey_definition.week_indices.first
      get :due, access_token: user.token, week_index: index

      expect(response.body).to eq(user_obligations.obligations_due(week_index: index).to_json)
    end
  end

  describe 'POST api/v1/surveys/submit/:name', wip: true do
    before do
      format = [{"name"=>"name1", "required"=>true, "type"=>"integer"}]

      @survey_definition.update(format: format)
    end

    describe 'invalid submissions' do
      before do
        @obligation = user.survey_obligations.search_by_survey_name(@survey_definition.name)
        submission = { 'name1' => 'true' }

        post :submit,
             access_token: user.token,
             name: @survey_definition.name,
             body: submission
      end
      it 'returns 422 HTTP' do
        expect(response.status).to eq(422)
      end

      it "doesn't persist the submission" do
        expect(@obligation.reload.submission).to be_nil
      end
    end

    describe 'valid submissions' do
      it 'saves the submission' do
        obligation = user.survey_obligations.search_by_survey_name(@survey_definition.name)
        submission = { 'name1' => '1' }

        post :submit,
             access_token: user.token,
             name: @survey_definition.name,
             body: submission

        expect(response.status).to eq(200)
        expect(obligation.reload.submission).to eq(submission)
      end
    end
  end
end
