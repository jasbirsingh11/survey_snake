require 'rails_helper'

describe SurveyDefinitionsController, type: :controller do
  let(:user) { FactoryGirl.create(:user_with_team) }
  let(:user_obligations) { user.survey_obligations }
  let(:survey) { user.survey_obligations.first }

  before do
    @survey_definition = FactoryGirl.create(:survey_definition)
  end

  describe 'api/v1/surveys/definitons/:id' do
    it 'returns a a given survey by a id' do
      get :show, id: @survey_definition.id, access_token: user.token

      expect(response.body).to eq(@survey_definition.to_json)
    end

    it 'returns a a given survey by a name' do
      get :show, id: @survey_definition.name, access_token: user.token

      expect(response.body).to eq(@survey_definition.to_json)
    end
  end

  describe 'api/v1/surveys/definitons/due_this_week' do
    before do
      survey.update(due_at: Date.today)
    end

    it 'returns the defintion of the survey' do
      get :due_this_week, access_token: user.token

      expect(response.body).to eq([@survey_definition].to_json)
    end
  end

  describe 'api/v1/surveys/definitons/submittable_now' do
    before do
      survey.update(submitted_at: nil, due_at: Date.today - 1, expires_at: Date.today + 1)
    end

    it 'returns the defintion of the survey' do
      get :submittable_now, access_token: user.token

      expect(response.body).to eq([@survey_definition].to_json)
    end
  end


  describe 'api/v1/surveys/defintions/missed' do
    before do
      survey.update(submitted_at: nil, expires_at: Date.today + 1)
    end

    it 'returns the defintion of the survey' do
      get :missed, access_token: user.token

      expect(response.body).to eq([@survey_definition].to_json)
    end
  end

  describe 'api/v1/surveys/defintions/missed' do
    it 'returns the defintion of the survey' do
      index = @survey_definition.week_indices.first
      get :due, access_token: user.token, week_index: index

      expect(response.body).to eq([@survey_definition].to_json)
    end
  end
end
