FactoryGirl.define do
  factory :team do
    name { Faker::Name.name }
    program_starts_at { Time.now + 1.day }
  end
end
